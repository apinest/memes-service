const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const MemeSchema = new Schema(
  {
    name: String,
    url: String,
    caption: String,
  },
  { timestamps: true }
);

module.exports = mongoose.model("Meme", MemeSchema);
